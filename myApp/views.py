from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import user, event, textQuestion, mcQuestion, choice, answerTQ, answerMQ, numPartner


################## (+1) or more partner #######################################################
def markUneditablePartner(request):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if 'event' not in request.COOKIES:
        return HttpResponse("You haven't selected an event yet! Please event an event.");
    try:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
    except:
        return HttpResponse("Invalid user or event.");
    else:
        curr_event.partner_editable = 0;
        curr_event.save();
        return HttpResponse("You have marked this question as uneditable!");


def editPartner(request):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if 'event' not in request.COOKIES:
        return HttpResponse("You haven't selected an event yet! Please event an event.");
    try:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        curr_numPartner = numPartner.objects.get(userKey = curr_user, eventKey = curr_event);
    except:
        return HttpResponse("Invalid user or event.");
    else:
        curr_numPartner.num = int(request.GET['num_partner']); curr_numPartner.save();
        context = {'user':curr_user, 'event':curr_event, 'partner_allowed':'yes', 'partner_editable' : 'yes', 'num_partner' : str(curr_numPartner.num)};
        return render(request, "myApp/answerPage.html", context);
    pass;


def addVendorPermissionPartner(request):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if 'event' not in request.COOKIES:
        return HttpResponse("You haven't selected an event yet! Please event an event.");
    try:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        curr_event.partner_seeable.add(user.objects.get(userID = request.GET['vendor_id']));
    except:
        return HttpResponse("Invalid user or event.");
    else:
        context = {'userID':curr_user.userID, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'add_vendor_permission' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
        


def allowPartner(request, flag):
    ##If flag = 1: Allow (+1) or more partners for this event.
    ##If flag = 0: Dis-allow (+1) or more partners for this event.
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    try:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
    except:
        return HttpResponse("Invalid user or event.");
    else:
        context = {'userID':request.COOKIES['userID'], 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all()};
        if flag == 1:
            curr_event.partner_allowed = 1; curr_event.save(); context['partner_allowed'] = 'yes';
        else:
            curr_event.partner_allowed = 0; curr_event.save();
        return render(request, "myApp/createEvent.html", context);

    

################### VENDOR CODE ################################################################
def markUneditable(request, flag, question_id):
    ##flag = 1: Mark the text question with question_id as uneditable.
    ##flag = 0: Mark the mutiple choice question with question_id as uneditable.
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if flag == 1:
        curr_question = textQuestion.objects.get(pk = question_id);
    else:
        curr_question = mcQuestion.objects.get(pk = question_id);
        pass;
    curr_question.editable = 0;
    curr_question.save();
    return HttpResponse("You have sucessfully marked that question as uneditable!");
        


def viewGuests(request, event_id):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    try:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_vendor.get(pk = event_id);
    except:
        return HttpResponse("Invalid userID, eventID, or you are not a vendor of this event!");
    else:
        if len(user.objects.filter(events_guest = curr_event)) == 0:
            context = {'no_guest' : 'yes'};
        else:
            context = {'guests' : user.objects.filter(events_guest = curr_event)};
        res = render(request, "myApp/guestList.html", context);
        res.set_cookie('event', curr_event.event_title);
        return res;
    pass;


def viewQuestionsAndResponses(request, guest_id):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    try:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_vendor.get(event_title = request.COOKIES['event']);
        guest = user.objects.get(id = guest_id);
    except:
        return HttpResponse("Invalid userID, guestID, eventID, or you are not a vendor of this event!");
    else:
        ##For all text questions.
        tqSet = curr_event.tq.filter(seeable = curr_user);
        mqSet = curr_event.mq.filter(seeable = curr_user);
        tqAnswers = set();
        mqAnswers = set();
        for tq in tqSet:
            tqAnswers.update([answerTQ.objects.get(userKey = guest, questionKey = tq)]);
        for mq in mqSet:
            mqAnswers.update([answerMQ.objects.get(userKey = guest, questionKey = mq)]);
            pass;
        context = {'tqAnswers':tqAnswers, 'mqAnswers' : mqAnswers, 'guest' : guest.userID, 'event' : curr_event.event_title};
        if len(curr_event.partner_seeable.filter(userID = curr_user.userID)) != 0:
            context['partner_seeable'] = 'yes';
            if curr_event.partner_allowed == 1:
                context['partner_allowed'] = 'yes';
                context['num_partner'] = str(numPartner.objects.get(userKey = guest, eventKey = curr_event).num);
        res = render(request, "myApp/viewQuestionsAndAnswers.html", context);
        res.set_cookie('guest', guest.userID);
        return res;


def addVendorPermission(request, question_id, flag):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if 'event' not in request.COOKIES:
        return HttpResponse("You haven't selected an event yet! Please event an event.");
    if 'vendor_id' not in request.GET or request.GET['vendor_id'] == '':
        return HttpResponse("Please enter the userID of the person you'll add as vendor!");
    try:
        u = user.objects.get(userID = request.GET['vendor_id']);
    except:
        return HttpResponse("The user you entered does not exist. Please go back and enter again!");
    else:
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        ##If the user is an owner.
        if len(u.events_vendor.filter(event_title = curr_event.event_title))==0:
            return HttpResponse("This user is NOT a vendor of this event! Please enter another uerID.");
        if len(u.events_potential_guest.filter(event_title = curr_event.event_title))!=0 or len(u.events_guest.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is a guest. Try another user.");
        if len(u.events_owner.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is an owner. Try another user.");
        if flag == 1: ##The question is a text question.
            curr_question = textQuestion.objects.get(pk = question_id);
        else:
            curr_question = mc.objects.get(pk = question_id);
            pass;
        curr_question.seeable.add(u); ##Now the vendor can view guests' reponses to this questions.
        context = {'userID':request.COOKIES['userID'], 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'add_vendor_permission' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    


def addVendor(request):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if 'event' not in request.COOKIES:
        return HttpResponse("You haven't selected an event yet! Please event an event.");
    if 'vendor_id' not in request.GET or request.GET['vendor_id'] == '':
        return HttpResponse("Please enter the userID of the person you'll add as vendor!");
    try:
        u = user.objects.get(userID = request.GET['vendor_id']);
    except:
        return HttpResponse("The user you entered does not exist. Please go back and enter again!");
    else:
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        ##If the user is an owner.
        if len(u.events_vendor.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is already an vendor of this event! Please enter another user.");
        if len(u.events_potential_guest.filter(event_title = curr_event.event_title))!=0 or len(u.events_guest.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is a guest. Try another user.");
        if len(u.events_owner.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is an owner. Try another user.");        
        ##The user is eligible for being a guest to this event.
        u.events_vendor.add(curr_event);
        context = {'userID':request.COOKIES['userID'], 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'add_vendor' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    pass;


##The following two functions changes the answer to text questions and multiple choice questions. 
def changeAnswerTQ(request):
    if 'userID' not in request.COOKIES or 'event' not in request.COOKIES or 'question' not in request.COOKIES:
        return HttpResponse("Invalid Request! Please login, select an event, or select a question!");
    else:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.tq.get(questionText = request.COOKIES['question']);
        if len(curr_user.allAnswers.filter(questionKey = curr_question)) == 0:
            new_answer = answerTQ.objects.create(userKey = curr_user, questionKey = curr_question, answerText = request.GET['new_answer']);
        else:
            original_answer = curr_user.allAnswers.get(questionKey = curr_question);
            original_answer.delete();
            new_answer = answerTQ.objects.create(userKey = curr_user, questionKey = curr_question, answerText = request.GET['new_answer']);
        context = {'user':curr_user, 'event':curr_event, 'question':curr_question, 'answer':new_answer, 'changed_tq':'yes'};
        return render(request, "myApp/currAnswer.html", context);
    pass;


def changeAnswerMQ(request):
    if 'userID' not in request.COOKIES or 'event' not in request.COOKIES or 'question' not in request.COOKIES:
        return HttpResponse("Invalid Request! Please login, select an event, or select a question!");
    else:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.mq.get(questionText = request.COOKIES['question']);
        if len(curr_user.allChoices.filter(questionKey = curr_question)) == 0:
            new_answer = answerMQ.objects.create(userKey = curr_user, questionKey = curr_question, choiceText = request.GET['choice']);
        else:
            original_answer = curr_user.allChoices.get(questionKey = curr_question);
            original_answer.delete();
            new_answer = answerMQ.objects.create(userKey = curr_user, questionKey = curr_question, choiceText = request.GET['choice']);
        context = {'user':curr_user, 'event':curr_event, 'question':curr_question, 'answer':new_answer, 'changed_mq':'yes'};
        return render(request, "myApp/currAnswer.html", context);
    pass;




##The following four functions are for view and changing answers to existing questions.
def viewAnswerTQ(request, question_id):
    if 'userID' not in request.COOKIES or 'event' not in request.COOKIES:
        return HttpResponse("Invalid Request!");
    else:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_guest.get(event_title = request.COOKIES['event']);
        curr_question = textQuestion.objects.get(pk = question_id);
        ##Two cases: Currently no answer or current there is an answer.
        if len(curr_user.allAnswers.filter(questionKey = curr_question)) == 0:
            context = {'user':curr_user, 'event':curr_event, 'question':curr_question, 'tq':'yes'};
        else:
            curr_answer = curr_user.allAnswers.get(questionKey = curr_question);
            context = {'user':curr_user, 'event':curr_event, 'question':curr_question, 'answer':curr_answer, 'tq':'yes'};
        if curr_question.editable == 1:
            context['editable'] = 'yse';
        res = render(request, "myApp/currAnswer.html", context);
        res.set_cookie('question', curr_question.questionText);
        return res;
    pass;


def viewAnswerMQ(request, question_id):
    if 'userID' not in request.COOKIES or 'event' not in request.COOKIES:
        return HttpResponse("Invalid Request!");
    else:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_guest.get(event_title = request.COOKIES['event']);
        curr_question = mcQuestion.objects.get(pk = question_id);
        if len(curr_user.allChoices.filter(questionKey = curr_question)) == 0:
            context = {'user':curr_user, 'event':curr_event, 'question':curr_question, 'mq':'yes'};
        else:
            curr_answer = curr_user.allChoices.get(questionKey = curr_question);
            context = {'user':curr_user, 'event':curr_event, 'question':curr_question, 'answer':curr_answer, 'mq':'yes'};
        if curr_question.editable == 1:
            context['editable'] = 'yse'; 
        res = render(request, "myApp/currAnswer.html", context);
        res.set_cookie('question', curr_question.questionText);
        return res;
    pass;



##This following view functions are for RSVPing for an un-responded event, and answering quesitons
def viewQuestions(request, event_id):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    try:
        curr_event = event.objects.get(pk = event_id);
    except:
        return HttpResponse("Invalid event ID!");
    else:
        if len(curr_event.user_guest.filter(userID = request.COOKIES['userID'])) == 0:
            return HttpResponse("You are not invited to this event!");
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        context = {'user':curr_user, 'event':curr_event};
        if curr_event.partner_allowed == 1:
            context['partner_allowed'] = 'yes';
            if curr_event.partner_editable == 1: context['partner_editable'] = 'yes';
            context['num_partner'] = str(numPartner.objects.get(userKey=curr_user, eventKey=curr_event).num);
        res = render(request, "myApp/answerPage.html", context);
        res.delete_cookie('question');
        res.set_cookie('event', curr_event.event_title);
        return res;
    pass;


def notAttending(request, event_id):  ##This means you are not attending the event -> stay in your login page (profile).
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    try:
        curr_event = event.objects.get(pk = event_id);
    except:
        return HttpResponse("Invalid event ID!");
    else:
        ##If the person is not invited to the event:
        if len(curr_event.user_potential_guest.filter(userID = request.COOKIES['userID'])) == 0:
            return HttpResponse("You are not invited to this event!");
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event.user_potential_guest.remove(curr_user);
        context = {'user':curr_user, 'not_attending':'yes'};
        return render(request, "myApp/loginPage.html", context);


def answerEvent(request, event_id):  ##This means you are attending the event -> go the the answer page to check and edit the questions.
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    try:
        curr_event = event.objects.get(pk = event_id);
    except:
        return HttpResponse("Invalid event ID!");
    else:
        ##If the person is not invited to the event:
        if len(curr_event.user_potential_guest.filter(userID = request.COOKIES['userID'])) == 0:
            return HttpResponse("You are not invited to this event!");
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event.user_potential_guest.remove(curr_user);
        curr_event.user_guest.add(curr_user);
        n = numPartner.objects.create(userKey = curr_user, eventKey = curr_event);
        context = {'user':curr_user, 'event':curr_event};
        if curr_event.partner_allowed == 1:
            context['partner_allowed'] = 'yes';
            if n.editable == 1: context['partner_editable'] = 'yes';
            context['num_partner'] = str(n.num);
        res = render(request, "myApp/answerPage.html", context);
        res.set_cookie('event', curr_event.event_title);
        return res;


##The following two views will invite another guest, and add another user as owner to an event. 
def inviteUser(request):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if 'event' not in request.COOKIES:
        return HttpResponse("You haven't selected an event yet! Please event an event.");
    if 'guest_id' not in request.GET or request.GET['guest_id'] == '':
        return HttpResponse("Please enter the userID of the person you'll invite!");
    try:
        u = user.objects.get(userID = request.GET['guest_id']);
    except:
        return HttpResponse("The user you entered does not exist. Please go back and enter again!");
    else:
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        ##If the user is an owner.
        if len(u.events_owner.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is an owner of this event! Please enter another user.");
        if len(u.events_potential_guest.filter(event_title = curr_event.event_title))!=0 or len(u.events_guest.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is already invited. Try another user.");
        ##The user is eligible for being a guest to this event.
        u.events_potential_guest.add(curr_event);
        context = {'userID':request.COOKIES['userID'], 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'invite' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    pass;


def addOwner(request):
    if 'userID' not in request.COOKIES:
        context = {'need_login' : 'yes'}; return render(request, "myApp/firstPage.html", context);
    if 'event' not in request.COOKIES:
        return HttpResponse("You haven't selected an event yet! Please event an event.");
    if 'owner_id' not in request.GET or request.GET['owner_id'] == '':
        return HttpResponse("Please enter the userID of the person you'll add as owner!");
    try:
        u = user.objects.get(userID = request.GET['owner_id']);
    except:
        return HttpResponse("The user you entered does not exist. Please go back and enter again!");
    else:
        curr_event = event.objects.get(event_title = request.COOKIES['event']);
        ##If the user is an owner.
        if len(u.events_owner.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is already an owner of this event! Please enter another user.");
        if len(u.events_potential_guest.filter(event_title = curr_event.event_title))!=0 or len(u.events_guest.filter(event_title = curr_event.event_title))!=0:
            return HttpResponse("This user is alreay invited as a guest. Try another user.");
        ##The user is eligible for being a guest to this event.
        u.events_owner.add(curr_event);
        context = {'userID':request.COOKIES['userID'], 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'add' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    pass;



##The following view functions allows one to add, delete choices for a multiple choice question,
##or change the content of an exist choice.
def backToEvent(request):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        if 'mc_question' in request.COOKIES:
            del request.COOKIES['mc_question'];
        context = {'userID':curr_user.userID, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'question_text' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    else:
        return HttpResponse("You haven't logged in! Pleae go to the login page.");
    pass;
    



def createChoice(request, question_id):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES and 'mc_question' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.mq.get(pk = question_id);
        curr_question.cc.create(choiceText = request.GET['choice_text']);
        context = {'userID':curr_user.userID, 'event':curr_event, 'curr_question':curr_question, 'choice_created':'yes'};
        return render(request, "myApp/editChoices.html", context);
    else:
        return HttpResponse("You haven't logged in! Please go to the login page.");
    pass;


def deleteChoice(request, choice_id):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES and 'mc_question' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.mq.get(questionText = request.COOKIES['mc_question']);
        curr_choice = curr_question.cc.get(pk = choice_id);
        curr_choice.delete();
        context = {'userID':curr_user.userID, 'event':curr_event, 'curr_question':curr_question, 'choice_deleted':'yes'};
        return render(request, "myApp/editChoices.html", context);
    else:
        return HttpResponse("You haven't logged in! Please go to the login page.");
    pass;


def changeChoiceText(request, choice_id):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES and 'mc_question' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.mq.get(questionText = request.COOKIES['mc_question']);
        curr_choice = curr_question.cc.get(pk = choice_id);
        curr_choice.choiceText = request.GET['Change Choice Text'];
        curr_choice.save();
        context = {'userID':curr_user.userID, 'event':curr_event, 'curr_question':curr_question, 'text_changed':'yes'};
        return render(request, "myApp/editChoices.html", context);
    else:
        return HttpResponse("You haven't logged in! Please go to the login page.");
    pass;



def editChoices(request, question_id):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.mq.get(pk = question_id);
        context = {'userID':curr_user.userID, 'event':curr_event, 'curr_question':curr_question};
        res =  render(request, "myApp/editChoices.html", context);
        res.set_cookie('mc_question', curr_question.questionText);
        return res;
    else:
        return HttpResponse("You haven't logged in! Please go to the login page.");

############################################################################################################
############################################################################################################
##This function changes the text for an existring text questi
def changeTextTQ(request, question_id):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.tq.get(pk = question_id);
        curr_question.questionText = request.GET['Change Question Text'];
        curr_question.save();
        #print(curr_question.questionText);
        context = {'userID':curr_user.userID, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'question_text' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    else:
        return HttpResponse("You haven't logged in! Pleae go to the login page.");
    pass;


##These two view functions are for deleting a text question, and a multiple choice question respectively.
def deleteTQ(request, question_id):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.tq.get(pk = question_id);
        curr_question.delete();
        context = {'userID':curr_user.userID, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'delete_tq_successful' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    else:
        return HttpResponse("You haven't logged in! Pleae go to the login page.");
    pass;

    
def deleteMQ(request, question_id):
    if 'userID' in request.COOKIES and 'event' in request.COOKIES:
        curr_user = user.objects.get(userID = request.COOKIES['userID']);
        curr_event = curr_user.events_owner.get(event_title = request.COOKIES['event']);
        curr_question = curr_event.mq.get(pk = question_id);
        curr_question.delete();
        context = {'userID':curr_user.userID, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'delete_mq_successful' : 'yes'};
        if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
        return render(request, "myApp/createEvent.html", context);
    else:
        return HttpResponse("You haven't logged in! Pleae go to the login page.");
    pass;


##This view function is to create a new text question.
def createTextQuestion(request):
    cookie_user = request.COOKIES['userID'];
    cookie_event = request.COOKIES['event'];
    question_text = request.GET['question_text'];
    #Create a new question for the event under the current user, then return to the 'createEvent' page.
    curr_user = user.objects.get(userID = cookie_user);
    curr_event = curr_user.events_owner.get(event_title = cookie_event);
    curr_event.tq.create(questionText = question_text);
    context = {'userID':cookie_user, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'text_question_created' : 'yes'};
    if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
    return render(request, "myApp/createEvent.html", context);


##This view function is to create a new multiple choice question.
def createMultipleChoiceQuestion(request):
    cookie_user = request.COOKIES['userID'];
    cookie_event = request.COOKIES['event'];
    question_text = request.GET['question_text'];
    choice_num = range(int(request.GET['Number of Choices']));
    #Create a new question for the event under the current user, then return to the 'createEvent' page.
    curr_user = user.objects.get(userID = cookie_user);
    curr_event = curr_user.events_owner.get(event_title = cookie_event);
    curr_question = curr_event.mq.create(questionText = question_text);
    context = {'userID':cookie_user, 'event':cookie_event, 'question':question_text, 'num':choice_num};
    res = render(request, "myApp/createChoice.html", context);
    res.set_cookie('num_choices', str(len(choice_num)));
    res.set_cookie('question', question_text);
    return res;

##This view is to create choices for a particular mc question under a user.
def createChoices(request):
    cookie_user = request.COOKIES['userID'];
    cookie_event = request.COOKIES['event'];
    cookie_question = request.COOKIES['question'];
    cookie_choice_num = request.COOKIES['num_choices'];
    #Create and store the choices to the database.
    curr_user = user.objects.get(userID = cookie_user);
    curr_event = curr_user.events_owner.get(event_title = cookie_event);
    print(curr_event);
    print(curr_event.mq.all());
    curr_question = curr_event.mq.get(questionText = cookie_question); 
    for i in range(int(cookie_choice_num)):
        c = 'choice' + str(i+1);
        curr_choice = request.GET[c];
        curr_question.cc.create(choiceText = curr_choice);
        
    context = {'userID':cookie_user, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all(), 'mc_question_created' : 'yes'};
    if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
    return render(request, "myApp/createEvent.html", context);


##This function will let you edit the details of a specific question as owner. 
def editEvent(request, event_id):
    cookie_user = request.COOKIES['userID'];
    curr_user = user.objects.get(userID = cookie_user);
    curr_event = curr_user.events_owner.get(pk = event_id);
    context = {'userID':cookie_user, 'event':curr_event, 'text_questions':curr_event.tq.all(),'multiple_choice_questions':curr_event.mq.all()};
    if curr_event.partner_allowed == 1: context['partner_allowed'] = 'yes';
    res = render(request, "myApp/createEvent.html", context);
    res.set_cookie('event', curr_event.event_title);
    return res;


##This view function is to create a new event.
##The userID is contained in the COOKIES field of request. 
def createEvent(request):
    if 'userID' in request.COOKIES:
        print('userID is still in cookies:');
        print(request.COOKIES['userID']);

        #User already logged in, create a new event for the user. 
        cookie_value = request.COOKIES['userID'];
        context = {'userID' : cookie_value};
        eventTitle = request.GET['event_title'];

        #Case 1: There is already an event with the same title in the database, or the date field entered is invalid Let the user retry.
        #Case 2: No event with the same name exists in the database. Event creation is allowed. 
        if len(event.objects.filter(event_title = eventTitle)) != 0:
            curr_user = user.objects.get(userID = cookie_value);
            context = {'user':curr_user, 'already_exists':'yes'};
            return render(request, "myApp/loginPage.html", context);
        else:
            u1 = user.objects.get(userID = cookie_value);
            u1.events_owner.create(event_title = eventTitle);
            ##Retrive all currently available text questions and multiple choice questions related to this event.
            curr_event = u1.events_owner.get(event_title = eventTitle);
            context = {'user':u1, 'created':'yes'};
            res = render(request, "myApp/loginPage.html", context);
            ##res.set_cookie('event', eventTitle);
            return res;
    else:
        #The user did not login, re-direct the user to the login page
        #print("Sorry, the input request does not contain the cookie.");
        context = {'need_login' : 'yes'};
        return render(request, "myApp/firstPage.html", context);
    pass;



##This is a function that takes you back to your profile. 
def backToProfile(request):
    if 'userID' in request.COOKIES:
        ##You have logged in, take you to your profile.
        cookie_user = request.COOKIES['userID'];
        curr_user = user.objects.get(userID = cookie_user);
        context = {'user' : curr_user};
        res = render(request, "myApp/loginPage.html", context);
        res.delete_cookie('event');
        res.delete_cookie('question');
        return res;
        pass;
    else:
        return HttpResponse("You haven't logged in! Please go to the login page.");



##This function is for logging out.
def logout(request):
    if 'userID' in request.COOKIES:
        del request.COOKIES['userID'];
    if 'event' in request.COOKIES:
        del request.COOKIES['event'];
    if 'mc_question' in request.COOKIES:
        del request.COOKIES['mc_question'];
    if 'question' in request.COOKIES:
        del request.COOKIES['question'];
    if 'num_choices' in request.COOKIES:
        del request.COOKIES['num_choices'];
    res = render(request, "myApp/firstPage.html", {});
    res.delete_cookie('userID');
    res.delete_cookie('event');
    res.delete_cookie('mc_question');
    res.delete_cookie('num_choices');
    return res;


##This view function is for logging in. 
def loginPage(request):
    input_name = request.GET['userID'];
    input_password = request.GET['passWord'];
    try: 
        Name = user.objects.get(userID = input_name);
    except (user.DoesNotExist):
        #Case 1: Account does not exist.
        print("The user with the given username does not exist.");
        print(input_name);
        template = loader.get_template("myApp/firstPage.html");
        context = {'exist' : 'no'};
        return HttpResponse(template.render(context, request));
    else:
        Name = user.objects.get(userID = input_name);
        #Case 2: Account exists but incorrect password.
        if input_password != Name.passWord:
            context = {'wrong_password':'yes'};
            template = loader.get_template("myApp/firstPage.html");
            return HttpResponse(template.render(context, request));
        #Case 3: Account exists and the password is correct. 
        else:
            u1 = user.objects.get(userID = input_name);
            context = {'user' : u1};
            res = render(request, "myApp/loginPage.html", context);
            res.set_cookie('userID', input_name); 
            #print("The cookie value is: "); print(res.);
            return res;

        
##This is the view function to create a new account. 
def createAccount(request):
    input_name = request.GET['userID'];
    input_password = request.GET['passWord'];

    try:
        Name = user.objects.get(userID = input_name);
    except (user.DoesNotExist):
        #The user name entered does not exist.
        #Create a new user and password. 
        new_username = user.objects.create(userID = input_name, passWord = input_password);
        #print(new_username.passWord);
        context = {'just_created':'yes'};
        template = loader.get_template('myApp/firstPage.html');
        return HttpResponse(template.render(context, request));
    else:
        #The user name exists.
        context = {'name_already_exist':'yes'};
        template = loader.get_template('myApp/firstPage.html');
        return HttpResponse(template.render(context, request));

    


##This is the first page of the website.(The login-or-create-account page).
def firstPage(request):
    template = loader.get_template('myApp/firstPage.html');
    context = {};
    return HttpResponse(template.render(context, request));


    
def deleteAll(request):
    answerTQ.objects.all().delete();
    answerMQ.objects.all().delete();
    choice.objects.all().delete();
    mcQuestion.objects.all().delete();
    textQuestion.objects.all().delete();
    event.objects.all().delete();
    user.objects.all().delete();
    template = loader.get_template('myApp/firstPage.html');
    context = {'deleted':'yes'};
    return HttpResponse(template.render(context, request));

    
