from django.db import models

# Create your models here.
class user(models.Model):
    userID = models.CharField(max_length = 20);
    passWord = models.CharField(max_length = 20);
    def __str__(self):
        return self.userID;


class event(models.Model):
    #userid = models.ForeignKey(user, on_delete = models.CASCADE);
    user_owner = models.ManyToManyField(user, related_name = 'events_owner');
    user_guest = models.ManyToManyField(user, related_name = 'events_guest');
    user_potential_guest = models.ManyToManyField(user, related_name = 'events_potential_guest');
    user_vendor = models.ManyToManyField(user, related_name = 'events_vendor');
    event_title = models.CharField(max_length = 200);
    event_date = models.IntegerField(default = 0);
    partner_allowed = models.IntegerField(default = 0);
    partner_seeable = models.ManyToManyField(user, related_name = 'seeable_partner');
    partner_editable = models.IntegerField(default = 1);
    def __str__(self):
        return self.event_title;

    
class numPartner(models.Model):
    userKey = models.ForeignKey(user, on_delete = models.CASCADE, related_name = 'partners');
    eventKey = models.ForeignKey(event, on_delete = models.CASCADE, related_name = 'num_partners');
    num = models.IntegerField(default = 0);
    seeable = models.ManyToManyField(user, related_name = 'seeable_np');  ##Useless field but don't delete.
    editable = models.IntegerField(default = 1); ##Useless field but don't delete.


class textQuestion(models.Model):
    eventKey = models.ForeignKey(event, on_delete = models.CASCADE, related_name = 'tq');
    questionText = models.CharField(max_length = 200);
    seeable = models.ManyToManyField(user, related_name = 'seeable_tq');
    editable = models.IntegerField(default = 1);
    def __str__(self):
        return self.questionText;


class mcQuestion(models.Model):
    eventKey = models.ForeignKey(event, on_delete = models.CASCADE, related_name = 'mq');
    questionText = models.CharField(max_length = 200);
    seeable = models.ManyToManyField(user, related_name = 'seeable_mq');
    editable = models.IntegerField(default = 1);
    def __str__(self):
        return self.questionText;
    

class choice(models.Model):
    questionKey = models.ForeignKey(mcQuestion, on_delete = models.CASCADE, related_name = 'cc');
    choiceText = models.CharField(max_length = 200);
    def __str__(self):
        return self.choiceText;
    pass;

class answerTQ(models.Model):
    userKey = models.ForeignKey(user, on_delete = models.CASCADE, related_name = 'allAnswers');
    questionKey = models.ForeignKey(textQuestion, on_delete = models.CASCADE, related_name = 'answer');
    answerText = models.CharField(max_length = 200);
    def __str__(self):
        return self.answerText;
    pass;


class answerMQ(models.Model):
    userKey = models.ForeignKey(user, on_delete = models.CASCADE, related_name = 'allChoices');
    questionKey = models.ForeignKey(mcQuestion, on_delete = models.CASCADE, related_name = 'choice');
    choiceText = models.CharField(max_length = 200);
    def __str__(self):
        return self.choiceText;
    pass;
